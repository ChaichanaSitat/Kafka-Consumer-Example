package com.kafka.consumer.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ConsumerListener {

    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;

    @Value("${spring.kafka.consumer.topic}")
    private String chaichanaTopic;

    @Async
    @KafkaListener(topics = "#{'${spring.kafka.consumer.topic}'}",
            groupId = "#{'${spring.kafka.consumer.group-id}'}")
    public void listener(String message) {

        log.info("Message: {}", message);
       
    }
}
