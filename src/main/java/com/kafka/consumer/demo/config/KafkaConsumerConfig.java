package com.kafka.consumer.demo.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaConsumerConfig {
	
	@Value("${spring.kafka.consumer.bootstrap-servers}")
	private String brokers;
	
	@Value("${spring.kafka.consumer.key-serializer}")
	private String kafkaKeySerializer;

	@Value("${spring.kafka.consumer.value-serializer}")
	private String kafkaValueSerializer;
	
	@Value("${spring.kafka.consumer.group-id}")
	private String groupId;

	
	  @Bean
	    public ConsumerFactory<String, String> tungngernConsumerFactory() {
	        Map<String, Object> props = new HashMap<>();
	        log.info("tungnernBootstrapAddress {}", brokers);
	        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
	        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
	        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,kafkaKeySerializer);
	        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,kafkaValueSerializer);
	        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
	        return new DefaultKafkaConsumerFactory<>(props);
	    }
	 
	    @Bean
	    public ConcurrentKafkaListenerContainerFactory<String, String> tnKafkaListenerContainerFactory() {
	        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
	        factory.setConsumerFactory(tungngernConsumerFactory());
	        return factory;
	    }
}
